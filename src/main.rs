use poise::serenity_prelude as serenity;
use std::{collections::HashMap, env};
use tokio::sync::Mutex;

mod commands;
use commands::*;

type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;

#[cfg(feature = "aichat")]
struct Data {
    airgacha_result: Mutex<HashMap<airgacha::AirGachaItem, airgacha::AirGachaRecord>>,
    ai_model: mistralrs::Model,
    ai_mutex: Mutex<()>,
}

#[cfg(feature = "aichat")]
async fn prepare_data() -> Data {
    let airgacha_result = (tokio::fs::read_to_string("data.json").await).map_or_else(
        |_| Mutex::new(HashMap::new()),
        |json| {
            let data: HashMap<airgacha::AirGachaItem, airgacha::AirGachaRecord> =
                serde_json::from_str(&json).unwrap();
            Mutex::new(data)
        },
    );
    let ai_model = mistralrs::GgufModelBuilder::new(
        "SakanaAI/TinySwallow-1.5B-Instruct-GGUF",
        vec!["tinyswallow-1.5b-instruct-q8_0.gguf"],
    )
    .with_tok_model_id("SakanaAI/TinySwallow-1.5B-Instruct")
    .build()
    .await
    .unwrap();

    Data {
        airgacha_result,
        ai_model,
        ai_mutex: Mutex::new(()),
    }
}

#[cfg(not(feature = "aichat"))]
struct Data {
    airgacha_result: Mutex<HashMap<airgacha::AirGachaItem, airgacha::AirGachaRecord>>,
}

#[cfg(not(feature = "aichat"))]
async fn prepare_data() -> Data {
    let airgacha_result = (tokio::fs::read_to_string("data.json").await).map_or_else(
        |_| Mutex::new(HashMap::new()),
        |json| {
            let data: HashMap<airgacha::AirGachaItem, airgacha::AirGachaRecord> =
                serde_json::from_str(&json).unwrap();
            Mutex::new(data)
        },
    );
    Data { airgacha_result }
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let token = env::var("RSBOT_TOKEN").expect("Expected a token in the environment variables");
    let intents =
        serenity::GatewayIntents::non_privileged() | serenity::GatewayIntents::MESSAGE_CONTENT;

    let options = poise::FrameworkOptions {
        commands: vec![
            basic::ping(),
            event::gakuryokuou(),
            dice::roll(),
            numgacha::numgacha(),
            airgacha::airgacha(),
            airgacha::airgacha_list(),
            airgacha::airgacha_statistics(),
            basic::say(),
            gosenchouyen::gosenchouyen(),
            ojichat::ojichat(),
            sort::sort(),
            aichat::aichat(),
            newyear::countdown(),
            abbrev::abbrev(),
        ],
        prefix_options: poise::PrefixFrameworkOptions {
            prefix: Some("!!".into()),
            ..Default::default()
        },
        ..Default::default()
    };
    let framework = poise::Framework::builder()
        .setup(|ctx, ready, framework| {
            Box::pin(async move {
                poise::builtins::register_globally(ctx, &framework.options().commands).await?;

                let data = prepare_data().await;
                println!("{} is ready!", ready.user.name);
                Ok(data)
            })
        })
        .options(options)
        .build();

    let client = serenity::ClientBuilder::new(token, intents)
        .framework(framework)
        .await;
    client.unwrap().start().await.unwrap();
}
