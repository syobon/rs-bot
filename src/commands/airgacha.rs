use chrono::{DateTime, Local};
use rand::seq::IndexedRandom;
use serde::{Deserialize, Serialize};

use crate::{Context, Error};

#[derive(Debug, Clone, Copy)]
#[allow(clippy::upper_case_acronyms)]
enum AirGachaRank {
    N = 1,
    UN = 2,
    R = 3,
    SR = 4,
    SSR = 5,
    UR = 6,
}

#[derive(Clone, Copy, Hash, PartialEq, Eq, Deserialize, Serialize)]
pub enum AirGachaItem {
    Nitrogen,
    Oxygen,
    Argon,
    CarbonDioxide,
    Neon,
    Helium,
    Methane,
    Krypton,
    SulfurDioxide,
    Hydrogen,
    DinitrogenMonoxide,
    Xenon,
    Ozone,
    NitrogenDioxide,
    Iodine,
    Chloromethane,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct AirGachaRecord {
    count: u64,
    first: Option<DateTime<Local>>,
}

fn airgacha_internal(count: usize) -> Vec<(AirGachaRank, AirGachaItem)> {
    let mut rng = rand::rng();
    let mut results = Vec::with_capacity(count);
    for _ in 0..count {
        let chosen = ITEM_LIST.choose_weighted(&mut rng, |item| item.2).unwrap();
        results.push((chosen.0, chosen.1));
    }
    results
}

const ITEM_LIST: [(AirGachaRank, AirGachaItem, u64); 16] = [
    (AirGachaRank::N, AirGachaItem::Nitrogen, 78_074_562_245),
    (AirGachaRank::UN, AirGachaItem::Oxygen, 20_947_600_000),
    (AirGachaRank::R, AirGachaItem::Argon, 934_000_000),
    (AirGachaRank::SR, AirGachaItem::CarbonDioxide, 41_000_000),
    (AirGachaRank::SR, AirGachaItem::Neon, 1_818_000),
    (AirGachaRank::SR, AirGachaItem::Helium, 524_000),
    (AirGachaRank::SR, AirGachaItem::Methane, 181_000),
    (AirGachaRank::SR, AirGachaItem::Krypton, 114_000),
    (AirGachaRank::SR, AirGachaItem::SulfurDioxide, 100_000),
    (AirGachaRank::SSR, AirGachaItem::Hydrogen, 50_000),
    (AirGachaRank::SSR, AirGachaItem::DinitrogenMonoxide, 32_000),
    (AirGachaRank::SSR, AirGachaItem::Xenon, 8_700),
    (AirGachaRank::SSR, AirGachaItem::Ozone, 7_000),
    (AirGachaRank::SSR, AirGachaItem::NitrogenDioxide, 2_000),
    (AirGachaRank::SSR, AirGachaItem::Iodine, 1_000),
    (AirGachaRank::UR, AirGachaItem::Chloromethane, 55),
];

const ITEM_LIST_STR: &str = r"```
N
　窒素(N₂)　　　　    78.074562245 (調整値)

UN
　酸素(O₂)　　　　    20.9476%

R
　アルゴン(Ar)　　    0.934%

SR
　二酸化炭素(CO₂)　   0.041%
　ネオン(Ne)　　　    0.001818%
　ヘリウム(He)　　    0.000524%
　メタン(CH₄)　　　   0.000181%
　クリプトン(Kr)　    0.000114%
　二酸化硫黄(SO₂)　   0.0001%

SSR
　水素(H₂)　　　　    0.00005%
　一酸化二窒素(N₂O)   0.000032%
　キセノン(Xe)　　    0.0000087%
　オゾン(O₃)　　　    0.000007%
　二酸化窒素(NO₂)　   0.000002%
　ヨウ素(I₂)　　　    0.000001%

UR
　クロロメタン(CH₃Cl) 0.000000055%
```";

fn airgacha_get_name(res: AirGachaItem) -> String {
    match res {
        AirGachaItem::Nitrogen => "窒素(N₂)",
        AirGachaItem::Oxygen => "酸素(O₂)",
        AirGachaItem::Argon => "アルゴン(Ar)",
        AirGachaItem::CarbonDioxide => "二酸化炭素(CO₂)",
        AirGachaItem::Neon => "ネオン(Ne)",
        AirGachaItem::Helium => "ヘリウム(He)",
        AirGachaItem::Methane => "メタン(CH₄)",
        AirGachaItem::Krypton => "クリプトン(Kr)",
        AirGachaItem::SulfurDioxide => "二酸化硫黄(SO₂)",
        AirGachaItem::Hydrogen => "水素(H₂)",
        AirGachaItem::DinitrogenMonoxide => "一酸化二窒素(N₂O)",
        AirGachaItem::Xenon => "キセノン(Xe)",
        AirGachaItem::Ozone => "オゾン(O₃)",
        AirGachaItem::NitrogenDioxide => "二酸化窒素(NO₂)",
        AirGachaItem::Iodine => "ヨウ素(I₂)",
        AirGachaItem::Chloromethane => "クロロメタン(CH₃Cl)",
    }
    .to_owned()
}

fn airgacha_get_name_with_pad(res: AirGachaItem) -> String {
    match res {
        AirGachaItem::Nitrogen => "窒素(N₂)　　　　    ",
        AirGachaItem::Oxygen => "酸素(O₂)　　　　    ",
        AirGachaItem::Argon => "アルゴン(Ar)　　    ",
        AirGachaItem::CarbonDioxide => "二酸化炭素(CO₂)　   ",
        AirGachaItem::Neon => "ネオン(Ne)　　　    ",
        AirGachaItem::Helium => "ヘリウム(He)　　    ",
        AirGachaItem::Methane => "メタン(CH₄)　　　   ",
        AirGachaItem::Krypton => "クリプトン(Kr)　    ",
        AirGachaItem::SulfurDioxide => "二酸化硫黄(SO₂)　   ",
        AirGachaItem::Hydrogen => "水素(H₂)　　　　    ",
        AirGachaItem::DinitrogenMonoxide => "一酸化二窒素(N₂O)   ",
        AirGachaItem::Xenon => "キセノン(Xe)　　    ",
        AirGachaItem::Ozone => "オゾン(O₃)　　　    ",
        AirGachaItem::NitrogenDioxide => "二酸化窒素(NO₂)　   ",
        AirGachaItem::Iodine => "ヨウ素(I₂)　　　    ",
        AirGachaItem::Chloromethane => "クロロメタン(CH₃Cl) ",
    }
    .to_owned()
}

/// 空気ガチャ
#[poise::command(slash_command, prefix_command, user_cooldown = 10)]
pub async fn airgacha(
    ctx: Context<'_>,
    #[description = "引く回数"]
    #[min = 1]
    #[max = 10]
    count: Option<usize>,
) -> Result<(), Error> {
    let count = count.unwrap_or(10).clamp(1, 10);
    let mut statistics = ctx.data().airgacha_result.lock().await;
    let results = airgacha_internal(count);
    let mut messages: Vec<String> = Vec::with_capacity(count);
    for result in results {
        statistics
            .entry(result.1)
            .and_modify(|record| record.count += 1)
            .or_insert_with(|| AirGachaRecord {
                count: 1,
                first: Some(Local::now()),
            });

        let rank = result.0;
        let item = result.1;
        let name = airgacha_get_name(item);
        let exclamation = "!".repeat(rank as usize);
        messages.push(format!(
            "{rank:?}{exclamation}\n{name}を引きました{exclamation}"
        ));
    }
    let json = serde_json::to_string(&*statistics)?;
    drop(statistics);
    tokio::fs::write("data.json", json).await?;
    if ctx.prefix() == "/" {
        ctx.say(messages.join("\n")).await?;
    } else {
        for message in messages {
            ctx.say(message).await?;
        }
    }
    Ok(())
}

/// 空気ガチャの統計を確認
#[poise::command(slash_command, prefix_command)]
pub async fn airgacha_statistics(ctx: Context<'_>) -> Result<(), Error> {
    let statistics = ctx.data().airgacha_result.lock().await;
    let mut messages: Vec<String> = Vec::with_capacity(ITEM_LIST.len() + 2);
    messages.push(String::from("```"));
    for (_, item, _) in ITEM_LIST {
        let record = statistics.get(&item).unwrap_or(&AirGachaRecord {
            count: 0,
            first: None,
        });
        let name = airgacha_get_name_with_pad(item);
        let count = record.count;
        let first = record.first.map_or(String::new(), |first| {
            first.format(" 初出: %Y/%m/%d %H:%M:%S").to_string()
        });
        messages.push(format!("{name}{count:>10}{first}"));
    }
    drop(statistics);
    messages.push(String::from("```"));
    ctx.say(messages.join("\n")).await?;
    Ok(())
}

/// 空気ガチャの排出率を確認
#[poise::command(slash_command, prefix_command)]
pub async fn airgacha_list(ctx: Context<'_>) -> Result<(), Error> {
    ctx.say(ITEM_LIST_STR).await?;
    Ok(())
}
