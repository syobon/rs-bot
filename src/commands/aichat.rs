use crate::{Context, Error};

/// AIと話します
#[cfg(feature = "aichat")]
#[poise::command(prefix_command)]
pub async fn aichat(
    ctx: Context<'_>,
    #[description = "プロンプト"]
    #[rest]
    prompt: String,
) -> Result<(), Error> {
    use mistralrs::{
        ChatCompletionChunkResponse, ChunkChoice, Delta, Response, TextMessageRole, TextMessages,
    };

    let discord_message = ctx.reply("-# 生成待機中…").await?;
    let flag = ctx.data().ai_mutex.lock().await;
    discord_message
        .edit(
            ctx,
            poise::CreateReply {
                content: Some("-# 生成中…".to_owned()),
                ..Default::default()
            },
        )
        .await?;
    let messages = TextMessages::new().add_message(TextMessageRole::User, &prompt);
    let mut response = String::new();
    let mut stream = ctx.data().ai_model.stream_chat_request(messages).await?;
    let mut edit_counter = 0;
    while let Some(chunk) = stream.next().await {
        if let Response::Chunk(ChatCompletionChunkResponse { choices, .. }) = chunk {
            if let Some(ChunkChoice {
                delta:
                    Delta {
                        content: Some(content),
                        ..
                    },
                ..
            }) = choices.first()
            {
                edit_counter += 1;
                response += content;
                if edit_counter >= 5 {
                    discord_message
                        .edit(
                            ctx,
                            poise::CreateReply {
                                content: Some(format!("{response}\n-# 生成中…")),
                                ..Default::default()
                            },
                        )
                        .await?;
                    edit_counter = 0;
                }
            }
        }
    }
    discord_message
        .edit(
            ctx,
            poise::CreateReply {
                content: Some(response),
                ..Default::default()
            },
        )
        .await?;

    drop(flag);
    Ok(())
}

/// AIと話します
#[cfg(not(feature = "aichat"))]
#[poise::command(prefix_command)]
pub async fn aichat(
    ctx: Context<'_>,
    #[description = "プロンプト"]
    #[rest]
    _prompt: String,
) -> Result<(), Error> {
    ctx.say("aichatは無効化されています。").await?;
    Ok(())
}
