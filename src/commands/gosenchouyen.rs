use poise::serenity_prelude as serenity;

use crate::{Context, Error};

/// 5000兆円欲しい！
#[poise::command(slash_command, prefix_command, broadcast_typing)]
pub async fn gosenchouyen(
    ctx: Context<'_>,
    #[description = "5000兆円"] upper: String,
    #[description = "欲しい！"] lower: String,
) -> Result<(), Error> {
    let url: &str = &format!("https://gsapi.cbrx.io/image?top={upper}&bottom={lower}");
    let mut attachment = serenity::CreateAttachment::url(ctx.http(), url).await?;
    attachment.filename = String::from("image.jpg");
    ctx.send(poise::CreateReply::default().attachment(attachment))
        .await?;
    Ok(())
}
