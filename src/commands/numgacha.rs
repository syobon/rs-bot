use rand::Rng;
use rand_distr::Geometric;

use crate::{Context, Error};

/// 数字ガチャ
#[poise::command(slash_command, prefix_command)]
pub async fn numgacha(
    ctx: Context<'_>,
    #[description = "確率 (%)"] percentage: Option<f64>,
) -> Result<(), Error> {
    if let Some(p) = percentage {
        if p <= 0. {
            ctx.say("確率は0より大きくしてください。").await?;
            return Ok(());
        }
        if 100. < p {
            ctx.say("確率は100%以下にしてください。").await?;
            return Ok(());
        }
    }
    let percent = percentage.unwrap_or(1.);
    let prob = percentage.map_or(0.01, |p| p / 100.);
    let expect = (1. / prob).round() as u64;

    // 1 / 2^54以下はハングする
    if prob <= 0.000_000_000_000_000_055_511_151_231_257_83 {
        ctx.say("確率が低すぎます。").await?;
        return Ok(());
    }

    let distr = Geometric::new(prob).unwrap();
    let result = {
        let mut rng = rand::rng();
        rng.sample(distr) + 1 // rand_distrのGeometricは「最初に成功するまでの失敗数」を返す
    };

    let message = format!(
        "確率{percent}%のガチャを出るまで回すと…\n{result}回目で当たりました！ (期待値: {expect})"
    );
    ctx.reply(message).await?;
    Ok(())
}
