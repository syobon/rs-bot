use rand::seq::IteratorRandom;
use unicode_segmentation::UnicodeSegmentation;

use crate::{Context, Error};

/// 略称を考えてくれます
#[poise::command(slash_command, prefix_command)]
pub async fn abbrev(
    ctx: Context<'_>,
    #[description = "略したいテキスト"]
    #[rest]
    content: String,
) -> Result<(), Error> {
    let graphemes: Vec<_> = content.grapheme_indices(true).collect();
    let max_length = (graphemes.len() / 2).max(1);
    let length = rand::random_range(1..=max_length);
    let mut picked = graphemes
        .into_iter()
        .choose_multiple(&mut rand::rng(), length);
    picked.sort_by_key(|g| g.0);
    let abbrev: String = picked.into_iter().map(|g| g.1).collect();
    ctx.say(format!("{content}の略称は{abbrev}です")).await?;
    Ok(())
}
