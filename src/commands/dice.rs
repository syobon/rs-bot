use rand::{distr::Uniform, Rng};
use rand_distr::Normal;

use crate::{Context, Error};

fn parse_dice(dice: &str) -> Result<(usize, u64), String> {
    let (count, face) = dice
        .split_once('d')
        .ok_or_else(|| String::from("ダイスは1d100の形式で入力してください"))?;
    let count: usize = count
        .parse()
        .map_err(|e| format!("ダイスの個数が不正です: {e}"))?;
    let face: u64 = face
        .parse()
        .map_err(|e| format!("ダイスの面数が不正です: {e}"))?;
    if count == 0 || face == 0 {
        return Err(String::from("個数と面数は1以上にしてください"));
    }
    Ok((count, face))
}

fn roll_internal(count: usize, face: u64) -> Result<String, Box<dyn std::error::Error>> {
    let mut rng = rand::rng();
    match count {
        1 => {
            let result = u128::from(rng.random_range(1..=face));
            Ok(format!("{face}面ダイスを振ると…\n{result}！"))
        }
        2..=10 => {
            let distribution = Uniform::new_inclusive(1u128, u128::from(face))?;

            let results: Vec<u128> = rng.sample_iter(distribution).take(count).collect();
            let result: u128 = results.iter().sum();
            Ok(format!(
                "{face}面ダイスを{count}回振ると…\n{results:?}\n合計 : {result}"
            ))
        }
        11..=16_777_215 => {
            let distribution = Uniform::new_inclusive(1u128, u128::from(face))?;

            let result: u128 = rng.sample_iter(distribution).take(count).sum();
            let average = result / count as u128;
            Ok(format!(
                "{face}面ダイスを{count}回振ると…\n合計 : {result}\n平均 : {average}"
            ))
        }
        _ => {
            let mean_single: f64 = (1. + face as f64) / 2.;
            let mean: f64 = mean_single * count as f64;
            let variation: f64 = 35. / 12. * count as f64;
            let normal_distr = Normal::new(mean, variation.sqrt())?;

            let result: f64 = rng.sample(normal_distr);
            let result: u128 = result.round() as u128;
            let average = result / count as u128;
            Ok(format!("{face}面ダイスを{count}回振ると…\n合計 : {result}\n平均 : {average}\n-# ⚠️回数の値が大きすぎるため、正規分布による近似値を使用しています。"))
        }
    }
}

/// ダイスロール
#[poise::command(slash_command, prefix_command)]
pub async fn roll(
    ctx: Context<'_>,
    #[description = "1d100形式のダイス"] dice: Option<String>,
) -> Result<(), Error> {
    let (count, face) = if let Some(dice) = dice {
        match parse_dice(&dice) {
            Ok(dice) => dice,
            Err(e) => {
                ctx.say(e).await?;
                return Ok(());
            }
        }
    } else {
        (1, 100)
    };

    let message = roll_internal(count, face).unwrap_or_else(|_| String::from("不正な入力です。"));

    ctx.say(message).await?;
    Ok(())
}
