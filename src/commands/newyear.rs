use chrono::prelude::*;
use std::time::Duration;
use tokio::time::sleep;

use crate::{Context, Error};

#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::cast_precision_loss)]
#[poise::command(prefix_command, required_permissions = "ADMINISTRATOR", hide_in_help)]
pub async fn countdown(ctx: Context<'_>) -> Result<(), Error> {
    let now = Local::now();
    let nextyear = now.year() + 1;
    let newyear = Local.with_ymd_and_hms(nextyear, 1, 1, 0, 0, 0).unwrap();
    loop {
        let now = Local::now();
        if now.nanosecond() < 1_000_000 {
            break;
        }
    }
    loop {
        let now = Local::now();
        let diff = newyear - now;
        let diff_secs: i32 = (diff.num_milliseconds() as f64 / 1000.).round() as i32;
        let diff_mins: i32 = (diff.num_seconds() as f64 / 60.).round() as i32;
        if diff_secs <= 0 {
            ctx.say(&format!("@everyone\nHappy New Year {nextyear} !!!"))
                .await?;
            break;
        }
        if diff_secs <= 60 {
            ctx.say(&format!("{nextyear}年まであと{diff_secs}秒"))
                .await?;
        } else if (diff_secs % 60) == 0 {
            ctx.say(&format!("{nextyear}年まであと{diff_mins}分"))
                .await?;
        }
        sleep(Duration::from_secs(1)).await;
    }
    Ok(())
}
