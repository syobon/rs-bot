use crate::{Context, Error};

/// PING
#[poise::command(slash_command, prefix_command)]
pub async fn ping(ctx: Context<'_>) -> Result<(), Error> {
    ctx.say("Pong!").await?;

    Ok(())
}

/// BOTに発言させます
#[poise::command(slash_command, prefix_command)]
pub async fn say(
    ctx: Context<'_>,
    #[description = "内容"]
    #[rest]
    content: String,
) -> Result<(), Error> {
    ctx.say(content).await?;
    Ok(())
}
