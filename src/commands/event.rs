use poise::serenity_prelude as serenity;
use std::env;

use crate::{Context, Error};

#[poise::command(prefix_command, hide_in_help)]
pub async fn gakuryokuou(ctx: Context<'_>) -> Result<(), Error> {
    let url = env::var("GAKURYOKUOU")?;
    ctx.author()
        .direct_message(ctx.http(), serenity::CreateMessage::new().content(url))
        .await?;

    Ok(())
}
