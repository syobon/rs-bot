use ojichat::ojichat;

use crate::{Context, Error};

/// おじさん構文
#[poise::command(slash_command, prefix_command)]
pub async fn ojichat(
    ctx: Context<'_>,
    #[description = "名前"]
    #[rest]
    name: Option<String>,
) -> Result<(), Error> {
    let target = name.unwrap_or(if let Some(author) = ctx.author_member().await {
        author
            .nick
            .clone()
            .map_or_else(|| ctx.author().name.clone(), |name| name)
    } else {
        ctx.author().name.clone()
    });
    let text = ojichat::get_message(Some(target), None, None);
    ctx.say(text).await?;
    Ok(())
}
