use unicode_segmentation::UnicodeSegmentation;

use crate::{Context, Error};

/// 文字列をソートします
#[poise::command(slash_command, prefix_command)]
pub async fn sort(
    ctx: Context<'_>,
    #[description = "文字列"]
    #[rest]
    text: String,
) -> Result<(), Error> {
    let mut vec: Vec<_> = text.graphemes(true).collect();
    vec.sort_unstable();
    let sorted = vec.join("");
    let message = format!("{text}\n↓sort\n{sorted}");
    ctx.say(message).await?;
    Ok(())
}
